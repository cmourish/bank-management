package com.banking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.banking.model.Banking;
import com.banking.service.BankService;



@RestController
@RequestMapping("/bnk")
public class Bankcontrol {
	
	@Autowired
	private BankService bservice;
	
	@PostMapping("/add") 
	public ResponseEntity<Banking> addAll(@RequestBody Banking banking){
		return ResponseEntity.ok(bservice.save(banking));
		
	}
	@GetMapping("/get")
	public ResponseEntity<List<Banking>> getAll(){
		return ResponseEntity.ok(bservice.findAll());
		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Banking> getAllbyId(@PathVariable Long id){
		return ResponseEntity.ok(bservice.findById((long) id).orElse(null) );
		
	}
	
	@PutMapping("/update")
	public ResponseEntity<Banking> updateAll(@RequestBody Banking banking){
		return ResponseEntity.ok(bservice.save(banking));
		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Banking> delete(@PathVariable Long id){
		bservice.findById(id).ifPresent(bservice::delete);
		return ResponseEntity.ok().build();
		
	}

}
